FROM openjdk:8
WORKDIR /app
COPY . /app
ENTRYPOINT [ "./mvnw", "spring-boot:run" ]